extern crate rfc4880;

use rfc4880::ascii_armor::*;
use std::collections::HashMap;

#[test]
fn test_armor_format() {
    let mut head = HashMap::new();
    head.insert("Version".to_string(), "Private Owl v0.1.0".to_string());
    head.insert("Comment".to_string(), "https://github.com/Diti/dev-rfc4880-rust/".to_string());

    let fake_armor = Armor {
        kind: ArmorKind::Message,
        headers: head,
        message: "Hi!".to_string(),
    };

    assert_eq!(fake_armor.to_string(), "----- BEGIN PGP MESSAGE -----
Comment: https://github.com/Diti/dev-rfc4880-rust/
Version: Private Owl v0.1.0

Hi!
----- END PGP MESSAGE -----");
}
