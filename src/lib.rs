/// "ASCII Armor" for Radix-64 encoded data.
/// See [RFC 4880, section 6.2](https://tools.ietf.org/html/rfc4880#section-6.2)
pub mod ascii_armor;


//#[test]
//fn new_armor() {
    // Radix-64 armor
    //let armor = new AsciiArmor()
    //.type(ArmorType::PublicKey)
    //.version(0.1.0)
    //.comment(b"Hello, I am an ASCII comment")
    // Optional 32-character string, mandatory for ArmorType::MultiPart
    //.messageId("") // Optional
//}
