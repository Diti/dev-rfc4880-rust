use std::collections::HashMap;
use std::fmt;
use std::fmt::Write;

const ARMOR_KIND_MSG       : &'static str = "MESSAGE";
const ARMOR_KIND_PUBKEY    : &'static str = "PUBLIC KEY BLOCK";
const ARMOR_KIND_PRIVKEY   : &'static str = "PRIVATE KEY BLOCK";
const ARMOR_KIND_SIG       : &'static str = "SIGNATURE";


/// The type of ASCII Armor
pub enum ArmorKind {
    /// Used for signed, encrypted, or compressed files
    Message,
    /// Used for armoring public keys
    PublicKey,
    /// Used for armoring private keys
    PrivateKey,
    /// Used for multi-part messages
    // If the number of parts `y` is unknown, a `MESSAGE-ID` armor header is required
    Multipart {
        /// The current number of message parts
        x: u32,
        /// The total number of message parts (can be unknown, in which case this number is ≤ 0)
        y: u32
    },
    /// Used for detached or cleartext signatures
    Signature
}

impl fmt::Debug for ArmorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", &self)
    }
}

impl fmt::Display for ArmorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", &self)
    }
}


/// The actual ASCII Armor
pub struct Armor {
    /// The kind of Armor (`type` was a reserved keyword)
    pub kind: ArmorKind,
    /// The armor headers
    pub headers: HashMap<String, String>,
    pub message: String,
}

/// The result of [`armor_to_string()`](#method.armor_to_string) for use in `{}`
impl fmt::Display for Armor {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", to_string(self))
    }
}

/// Returns a string representation of an ASCII Armor
pub fn to_string(armor: &Armor) -> String {
    let mut buf    = String::new();
    let armor_type = armor_line(&armor.kind);

    writeln!(buf, "----- BEGIN PGP {} -----", armor_type).unwrap();
    for (header, value) in &armor.headers {
        writeln!(buf, "{}: {}", &header, &value).unwrap();
    }

    write!(buf, "
{message}
----- END PGP {} -----", armor_type, message = &armor.message).unwrap();

    buf
}

/// Returns a string representation of an `ArmorKind`
fn armor_line(kind: &ArmorKind) -> String {
    match *kind {
        ArmorKind::Message                       => ARMOR_KIND_MSG.to_string(),
        ArmorKind::PublicKey                     => ARMOR_KIND_PUBKEY.to_string(),
        ArmorKind::PrivateKey                    => ARMOR_KIND_PRIVKEY.to_string(),
        /// If `x` is empty, we assume a `ArmorKind::Message` instead
        ArmorKind::Multipart { x, .. } if x == 0 => ARMOR_KIND_MSG.to_string(),
        /// If `y` is empty, it is a multipart message with unknown parts
        ArmorKind::Multipart { x, y }  if y == 0 => format!("MESSAGE, PART {}", x),
        /// A multipart message with known parts
        ArmorKind::Multipart { x, y }            => format!("MESSAGE, PART {}/{}", x, y),
        ArmorKind::Signature                     => ARMOR_KIND_SIG.to_string(),
    }
}

#[test]
fn armor_line_basic() {
    assert_eq!(armor_line(&ArmorKind::Message), "----- BEGIN PGP MESSAGE -----");
    assert_eq!(armor_line(&ArmorKind::PublicKey), "----- BEGIN PGP PUBLIC KEY BLOCK -----");
    assert_eq!(armor_line(&ArmorKind::PrivateKey), "----- BEGIN PGP PRIVATE KEY BLOCK -----");
}

#[test]
fn armor_line_multipart() {
    let with_y_known   = ArmorKind::Multipart { x: 1, y: 3 } ;
    let with_y_unknown = ArmorKind::Multipart { x: 1, y: 0 } ;

    assert_eq!(armor_line(&with_y_known), "----- BEGIN PGP MESSAGE, PART 1/3 -----");
    assert_eq!(armor_line(&with_y_unknown), "----- BEGIN PGP MESSAGE, PART 1 -----");
}
